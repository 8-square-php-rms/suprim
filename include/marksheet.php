<link rel="stylesheet" href="../Bootstrap4/css/bootstrap.min.css">

<?php include_once("config.php"); ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Page Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<script src="../assets/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../assets/main.css">
</head>

<body>

	<?php include('header.php');?>

	<div class="container">
		<section>

			<h2> STUDENT MARKSHEET</h2>

			<?php
										$id=$_POST['id'];
										$term_id = $_POST['term_id'];
										$class_id = $_POST['class_id'];
					
					
			?>

			<div class="table">
				<table>
					<br>
					<tr>
						<td>Student Name:
							<?php 
						$sql = "SELECT id,student_name FROM student WHERE id=$id";
						$result = $conn->query($sql);
						if ($result->num_rows > 0) {

							while ($row = mysqli_fetch_array($result)) {
								echo $row['student_name'] ;
							}
							mysqli_free_result($result);
						} else {
							echo "0 results";
						}
					 ?>
						</td>
					</tr>

					<tr>
						<td>Class Name:
							<?php 
								$sql = "SELECT class_id,class_grade FROM class WHERE class_id=$class_id";
								$result = $conn->query($sql);
								if ($result->num_rows > 0) {

									while ($row = mysqli_fetch_array($result)) {
										echo $row['class_grade'] ;
									}
									mysqli_free_result($result);
								} else {
									echo "0 results";
								}
							?>
						</td>
					</tr>

					<tr>
						<td>Terminal Exam:
							<?php 
								$sql = "SELECT term_id,term_type FROM term WHERE term_id=$term_id";
								$result = $conn->query($sql);
								if ($result->num_rows > 0) {

									while ($row = mysqli_fetch_array($result)) {
										echo $row['term_type'] ;
									}
									mysqli_free_result($result);
								} else {
									echo "0 results";
								}
							?>
						</td>
					</tr>
					<br>

					<tr bgcolor='#999a9b'>
						<td>Subject ID</td>
						<td>Subject</td>
						<td>Marks</td>
					</tr>
					<?php  

										$sql = "SELECT m.*, s.subject_name, std.id, c.class_id, t.term_id
													FROM marks m
													INNER JOIN subject s ON s.subject_id= m.subject_id
													INNER JOIN student std ON std.id = m.id
													INNER JOIN class c ON c.class_id = m.class_id
													INNER JOIN term t ON t.term_id = m.term_id
													WHERE std.id='$id'";
										$result = mysqli_query($conn, $sql);   
										$count=0;    	
						
										if ($result->num_rows > 0) {

											while($res = mysqli_fetch_array($result)) {         

												echo "<tr>";
												echo "<td>".$res['subject_id']."</td>";
												echo "<td>".$res['subject_name']."</td>";
												echo "<td>".$res['marksvalue']."</td>";
												echo "</tr>";  
												$count = $count + 1;
												
												$obtainmarks = $obtainmarks + $res['marksvalue'];
											}
											$newcount=$count*100;
											$percentage = (($obtainmarks/$newcount)*100);

											mysqli_free_result($result);
										} else {
											echo "0 results";
										}

							mysqli_close($conn);


					?>
					
				</table>
			</div>
		</section>
	<?php							
	echo $obtainmarks;
	echo "<br>";
	echo $percentage;
	?>
	</div>
</body>

</html>