<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <script src="../assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../assets/main.css">
</head>

<body>

    <?php include('header.php');
            include_once("config.php"); ?>

    <div class="container">

    
        <form method="POST" action="marksheet.php">

        <h2>Enter Student's Information</h2>

        <br>
            <div class="form-row">
                <div class="form-group col">
                    <label for="inputId">Student Name</label>

                    <select name="id" class="form-control">
                            <?php
                            $sql = "SELECT id,student_name FROM student";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=". $row['id'] .">" . $row['student_name'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            ?>
                        </select>

                </div>
            </div>


            <div class="form-row">
                <div class="form-group col">
                    <label for="inputTermd">Term Name</label>
                    <select name="term_id" class="form-control">
                            <?php
                            $sql = "SELECT term_id,term_type FROM term";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=". $row['term_id'] .">" . $row['term_type'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            ?>
                        </select>
                </div>
            </div>


            <div class="form-row">
                <div class="form-group col">
                <label for="inputClass">Class Name</label>
                    <select name="class_id" class="form-control">
                            <?php
                            $sql = "SELECT class_id,class_grade FROM class";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=". $row['class_id'] .">" . $row['class_grade'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }

							mysqli_close($conn);

                            ?>
                        </select>
                </div>
            </div>
            <!-- <div class="form-row">
                <div class="form-group col">
                    <label for="inputname">Student Name</label>
                    <input type="name" name="student_name" class="form-control" id="inputStduentName" placeholder="Student's Full Name">
                </div>
            </div>
             -->
            <!-- <div class="form-row">
                <div class="form-group col">
                    <label for="inputterm">Terminal Exam</label>
                    <input type="term" name="term_type" class="form-control" id="inputTerm" placeholder="Terminal Examination">
                </div>
            </div> -->

            <button type="submit" name="submit" value="submit" class="btn btn-primary">View Result</button>

        </form>
    </div>
</body>

</html>